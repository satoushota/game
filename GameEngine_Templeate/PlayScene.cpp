#pragma once
#include "PlayScene.h"



//コンストラクタ
PlayScene::PlayScene(IGameObject *parent)
	: IGameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	//弾の生成
	pBullet = CreateGameObject<Bullet>(this);
	
	//地面の生成
	pGround = CreateGameObject<Ground>(this);
}

//更新
void PlayScene::Update()
{
}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}