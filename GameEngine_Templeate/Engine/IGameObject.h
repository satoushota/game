#pragma once
#include<d3dx9.h>
#include<list>
#include<string>

//重力加速度
const float GRAVITY_ACCELERATION = 9.8f;

//地面の高さ
const float GROUND_HEIGHT = -0.5f;

class Collider;

//ゲームオブジェクトクラス
class IGameObject
{
protected:	

	//親の情報
	IGameObject* pParent_;
	
	//コライダー
	Collider* pCollider_;

	//子供の情報
	std::list<IGameObject*> childList_;
	
	//オブジェクトの名前
	std::string name_;

	//位置、角度、大きさ
	D3DXVECTOR3 position_;
	D3DXVECTOR3 rotate_;
	D3DXVECTOR3 scale_;

	//自分の行列用
	D3DXMATRIX localMatrix_;

	//今いるシーンの行列
	D3DXMATRIX worldMatrix_;

	//削除（）のフラグ
	bool dead_;

	//
	void Transform();

public:
	
	//コンストラクタ
	IGameObject();
	IGameObject(IGameObject* parent);
	IGameObject(IGameObject* parent, const std::string& name);
	
	//デストラクタ
	virtual ~IGameObject();

	template <class T>
	T* CreateGameObject(IGameObject* parent)
	{
		//親の情報を取得
		T* p = new T(parent);
		//親の子供情報にこれをいれる
		parent->PushBackChild(p);
		p->Initialize();
		return p;
	}

	//初期化
	virtual void Initialize() = 0;

	//あたり判定時の処理用仮想関数CLEAR
	virtual void OnCollision(IGameObject* pTarget) {};

	//更新、子クラスの更新用
	virtual void Update() = 0;
	void UpdateSub();

	//描画、子クラスの描画用
	virtual void Draw() = 0;
	void DrawSub();

	//開放、子クラスの解放用
	virtual void Release() = 0;
	void ReleaseSub();
	
	//当たり判定
	void Collision(IGameObject* targetOblect);
	
	//削除のフラグを立てる
	void KillMe();

	//
	void PushBackChild(IGameObject* obj);
		
	//各種ゲッター
	D3DXVECTOR3 GetPosition();

	D3DXVECTOR3 GetRotate();

	D3DXVECTOR3 GetScale();

	IGameObject* GetParent();

	const std::string& GetObjectName(void) const;

	//各種セッター
	void SetPosition(D3DXVECTOR3 position);

	void SetRotate(D3DXVECTOR3 rotate);

	void SetScale(D3DXVECTOR3 scale);

	void SetCollider(D3DXVECTOR3 &center, float radius);

};

