#include "IGameObject.h"
#include "SceneManager.h"
#include "Collider.h"

//コンストラクタ(親も名前もなし)
IGameObject::IGameObject() :
	IGameObject(nullptr, "")
{
}

//コンストラクタ（名前なし）
IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")
{
}

//コンストラクタ（標準）
IGameObject::IGameObject(IGameObject * parent, const std::string & name) :
	pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)),
	pCollider_(nullptr),dead_(false)
{
	
}

void IGameObject::Transform()
{
	//移動行列、回転行列(x,y,z)、拡大行列
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;

	//移動行列の作成
	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);

	//回転行列の作成
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));

	//拡大行列の作成
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);

	//各行列の合成
	localMatrix_ = matS * matRX * matRY *matRZ * matT;

	//
	if (pParent_ == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}
	else
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
	
}

//デストラクタ
IGameObject::~IGameObject()
{
	SAFE_DELETE(pCollider_);
}


//子の更新
void IGameObject::UpdateSub()
{
	Update();
	
	Transform();

	Collision(SceneManager::GetCurrentScene());

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{

		//削除フラグが真なら
		if ((*it)->dead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			it = childList_.erase(it);
		}
		else
		{
			it++;
		}
	}
}

//子の描画
void IGameObject::DrawSub()
{
	Draw();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
	}
}

//子の解放
void IGameObject::ReleaseSub()
{
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}


void IGameObject::KillMe()
{
	dead_ = true;
}

void IGameObject::PushBackChild(IGameObject* pObj)
{
	childList_.push_back(pObj);
}

void IGameObject::Collision(IGameObject* targetOblect)
{
	/*
		自分に当たり判定があり
			当たった相手が自分ではなく
				当たった相手の当たり判定
					相手と自分がぶつかっていたなら
	*/
	if (targetOblect != this &&
		this->pCollider_ != nullptr &&
		targetOblect->pCollider_ != nullptr &&
		pCollider_->IsHit(*targetOblect->pCollider_))
	{
		OnCollision(targetOblect);
	}
	
	for(auto itr = targetOblect->childList_.begin();
		itr != targetOblect->childList_.end();itr++)
	{
		Collision(*itr);
	}
}

const std::string & IGameObject::GetObjectName(void) const
{
	return name_;
}

IGameObject * IGameObject::GetParent()
{
	return pParent_;
}

D3DXVECTOR3 IGameObject::GetRotate()
{
	return rotate_;
}

D3DXVECTOR3 IGameObject::GetScale()
{
	return scale_;
}

D3DXVECTOR3 IGameObject::GetPosition()
{
	return position_;
}

void IGameObject::SetPosition(D3DXVECTOR3 position)
{
	position_ = position;
}

void IGameObject::SetRotate(D3DXVECTOR3 rotate)
{
	rotate_ = rotate;
}

void IGameObject::SetScale(D3DXVECTOR3 scale)
{
	scale_ = scale;
}

void IGameObject::SetCollider(D3DXVECTOR3 &center, float radius)
{
	pCollider_ = new Collider(this, center, radius);
}