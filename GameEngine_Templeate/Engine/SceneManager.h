#pragma once
#include "IGameObject.h"

//シーンの番号
enum SCENE_ID
{
	SCENE_ID_PLAY,
};

//◆◆◆を管理するクラス
class SceneManager : public IGameObject
{
	//現在のシーン
	static SCENE_ID currentSceneID_;

	//次のシーン
	static SCENE_ID nextSceneID_;

	//現在のシーンのポインタ
	static IGameObject* pCurrentScene_;

public:

	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//次のシーンの情報をもらう関数
	static void ChangeScene(SCENE_ID next);

	static IGameObject* GetCurrentScene();
};
