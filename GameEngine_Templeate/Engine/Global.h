#pragma once
#include <d3dx9.h>
#include<assert.h>
#include"Input.h"
#include"IGameObject.h"

#define SAFE_DELETE(p) if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if(p != nullptr){ delete[] p; p = nullptr;}
#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;}


struct Global
{
	int screenWidth;	//ウィンドウの幅
	int screenHeight;	//ウィンドウの横幅
};

extern Global g;


//extern const int	WINDOW_WIDTH = 800;	 //ウィンドウの幅
//extern const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ
