#include "Ground.h"
#include "Engine/Model.h"

//コンストラクタ
Ground::Ground(IGameObject * parent)
	:IGameObject(parent, "Ground"),hModel_(-1)
{
}

//デストラクタ
Ground::~Ground()
{
}

//初期化
void Ground::Initialize()
{
	//モデルの読み込み
	hModel_ = Model::Load("Data/model/Ground.fbx");
	assert(hModel_ != -1);

	//高さの設定
	position_.y = GROUND_HEIGHT;
}

//更新
void Ground::Update()
{
	if (Input::IsKey(DIK_A))
	{
		rotate_.z += 0.9;
	}
	if (Input::IsKey(DIK_D))
	{
		rotate_.z -= 1;
	}
}

//描画
void Ground::Draw()
{
	//モデルの描画
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Ground::Release()
{
}