#pragma once
#include"Engine/Global.h"
#include "Bullet.h"
#include "Ground.h"

class Bullet;

class Ground;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{

	Bullet* pBullet;
	Ground* pGround;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};