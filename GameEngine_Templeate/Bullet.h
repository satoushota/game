#pragma once
#include "Engine/IGameObject.h"



//◆◆◆を管理するクラス
class Bullet : public IGameObject
{
	//モデル番号
	int hModel_;
	
	//質量
	float mass_;

	//重さ
	float weight_;

	//半径（球）
	float radius_;

	//仮
	float angle_;

public:
	//コンストラクタ
	Bullet(IGameObject* parent);

	//デストラクタ
	~Bullet();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	bool CheckForMotion(float fricCoeff);

	float calcAccel(float fricCoeff);
};
