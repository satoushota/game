#include "Bullet.h"
#include "Engine/Model.h"

//コンストラクタ
Bullet::Bullet(IGameObject * parent)
	:IGameObject(parent, "Bullet"),hModel_(-1),mass_(0.0f),radius_(0.0f),weight_(0.0f),angle_(0.0f)
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルの読み込み
	hModel_ = Model::Load("Data/model/cube.fbx");
	assert(hModel_ != -1);

	//半径
	radius_ = 0.5f;

	//質量の設定
	mass_ = 0.1f;

	//重さの計算
	weight_ = mass_ * GRAVITY_ACCELERATION;
}

//更新
void Bullet::Update()
{
	//垂直抗力
	if (CheckForMotion(0.6))
	{
		//重さを加える
		position_.x -= calcAccel(0.6);
		position_.y -= calcAccel(0.6);
	}	

	if (Input::IsKey(DIK_A))
	{
		angle_ += 1.0;
		
		rotate_.z += 1;
		
	}
	if (Input::IsKey(DIK_D))
	{
		angle_ -= 1;
	}


	//重力
	/*position_.y -= weight_;*/

}

//描画
void Bullet::Draw()
{
	//モデルの描画
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}

bool Bullet::CheckForMotion(float fricCoeff)
{
	//PIパイ
	float normal = weight_ * cosf(angle_ * 3.14f / 180);

	float perpForce = weight_ * sinf(angle_ * 3.14 / 180);

	float stat_friction = fricCoeff * normal;

	return perpForce > stat_friction;
}

float Bullet::calcAccel(float fricCoeff)
{
	float normal = weight_ * cosf(angle_ * 3.14 / 180);

	float perForce = weight_ * sinf(angle_ * 3.14 / 180);

	float kin_friction = fricCoeff * normal;

	float totalForce = perForce - kin_friction;

	return totalForce / mass_;
}
