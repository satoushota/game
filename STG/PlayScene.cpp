#pragma once
#include "PlayScene.h"
#include "Bullet.h"
#include "Ground.h"
#include "Table.h"
#include "Player.h"
#include "Rack.h"
#include "Engine/SceneManager.h"
#include "Engine/Image.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject *parent)
	: IGameObject(parent, "PlayScene"), changeScene_(false)
{
}

//初期化
void PlayScene::Initialize()
{

	//プレイヤーを生成
	pPlayer_ = CreateGameObject<Player>(this);

	//地面の生成
	pGround_ = CreateGameObject<Ground>(this);

	//台の生成
	pTable_ = CreateGameObject<Table>(this);

	//商品棚の生成
	pRack_ = CreateGameObject<Rack>(this);

	//スコアの生成
	pScore_ = CreateGameObject<Score>(this);

}

//更新
void PlayScene::Update()
{
	//シーンを切り替える
	if (changeScene_)
	{
		time_ += 0.1;

		if (time_ > 30.0)
		{
			SceneManager::ChangeScene(SCENE_ID_RESULT);
		}
	}

}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
	
}

void PlayScene::ChangeScene()
{
	//スコアを記録
	pScore_->SaveScoreFile("Data/Score.txt");

	//シーンを移動
	changeScene_ = true;
}
