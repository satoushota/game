#include "Rack.h"
#include "Engine/Model.h"
#include  "Toy.h"

//コンストラクタ
Rack::Rack(IGameObject * parent)
	:IGameObject(parent, "Rack"),hModel_(-1)
{
}

//デストラクタ
Rack::~Rack()
{
}

//初期化
void Rack::Initialize()
{
	hModel_ = Model::Load("Data/Model/Play_Rack.fbx");
	assert(hModel_ != -1);

	position_ = D3DXVECTOR3(0, 0, -3);

	//棚に的を配置
	for (int i = -2; i <= 2; i+=2)
	{
		CreateGameObject<Toy>(pParent_)->SetPosition(position_ + D3DXVECTOR3(i, 2.1f, 0.0f));
	}
}

//更新
void Rack::Update()
{
}

//描画
void Rack::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Rack::Release()
{
}
