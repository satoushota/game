#include "Player.h"
#include "Engine/Camera.h"

//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player")
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//カメラの設定
	pCamera_ = CreateGameObject<Camera>(this);
	pCamera_->SetTarget(D3DXVECTOR3(0, 0, 1));

	//銃の設定
	pRifle_ = CreateGameObject<Rifle>(this);

	//プレイヤーの位置（視点）
	position_ = D3DXVECTOR3(0, 2, 1);

	//初期の向き
	rotate_.y = 180;
}

//更新
void Player::Update()
{
	Move();
}

void Player::Move()
{
	//キャラ移動
	//Aをおしたら左に移動
	if (Input::IsKey(DIK_A))
	{
		position_.x += 0.01f;
	}
	//Dをおしたら右に移動
	if (Input::IsKey(DIK_D))
	{
		position_.x -= 0.01f;
	}

	//視点
	//←をおしたら左視点移動
	if (Input::IsKey(DIK_LEFT) && rotate_.y > 90)
	{
		rotate_.y -= 0.2f;
	}
	//→をおしたら右視点移動
	if (Input::IsKey(DIK_RIGHT) && rotate_.y < 270)
	{
		rotate_.y += 0.2f;
	}
	//↑をおしたら上視点移動
	if (Input::IsKey(DIK_UP) && rotate_.x > -90)
	{
		rotate_.x -= 0.2f;
	}
	//↓をおしたら下視点移動
	if (Input::IsKey(DIK_DOWN) && rotate_.x < 88)
	{
		rotate_.x += 0.2f;
	}

}

//描画
void Player::Draw()
{
}

//開放
void Player::Release()
{
}