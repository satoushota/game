#pragma once

#include "Ground.h"

 namespace physics
{
	//反射ベクトルの計算
	//引数 : 進行方向ベクトル,法線ベクトル
	//戻り値 : 正規化反射方向ベクトル
	D3DXVECTOR3 CalcReflection(D3DXVECTOR3 moveDir, D3DXVECTOR3 normal);
};

