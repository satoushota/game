#pragma once
#include "Engine/global.h"
#include "Score.h"

class Score;

//リザルトシーンを管理するクラス
class ResultScene : public IGameObject
{
	//スコア
	Score* pScore_;

	//時間の計測をするかどうか
	bool countStop_;
public:
  //コンストラクタ
  //引数：parent  親オブジェクト（SceneManager）
  ResultScene(IGameObject* parent);

  //初期化
  void Initialize() override;

  //更新
  void Update() override;

  //描画
  void Draw() override;

  //開放
  void Release() override;
};