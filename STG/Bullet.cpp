#include "Bullet.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
#include "Engine/PlaneCollider.h"
#include "Rifle.h"
#include "physics.h"

//コンストラクタ
Bullet::Bullet(IGameObject * parent)
	:IGameObject(parent, "Bullet"),hModel_(-1),force_(D3DXVECTOR3(0,0,0)),moveDir_(D3DXVECTOR3(0, 0, 0)),acceleration_(D3DXVECTOR3(0, 0, 0)),
	velocity_(D3DXVECTOR3(0, 0, 0)),mass_(0)
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルの読み込み
	hModel_ = Model::Load("Data/model/Play_bullet.fbx");
	assert(hModel_ != -1);

	//あたり判定を作成
	SphereCollider * collider = new SphereCollider(this,position_,0.1f);
	SetCollider(collider);

	//撃った銃のポインタを取得
	IGameObject* pRifle = pParent_->FindChildObject("Rifle");

	//銃口の向きをもらって飛ぶ方向を決定
	moveDir_ = ((Rifle*)pRifle)->GetBulletMove_();
	
	//重さを設定（g）
	mass_ = 1;

	//飛ぶ方向をセット
	force_ = moveDir_ * 3;
}

//更新
void Bullet::Update()
{
	//力を合成
	force_  += GRAVITY_ACCELERATION;

	//加速度を計算
	acceleration_ = force_ * mass_;

	//速度を計算
	velocity_ += acceleration_;

	//位置を更新
	position_ += M_S(velocity_);

}

//描画
void Bullet::Draw()
{
	//モデルの描画
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}

void Bullet::OnCollision(IGameObject * pTarget, Collider* pMyCollider, Collider* pCollider)
{
	//法線ベクトル
	D3DXVECTOR3 normal;

	D3DXVECTOR3 reflection;

	switch (pCollider->GetShape())
	{
	//なにもなし（今のところ）
	case SPHERE: break;
	
	case PLANE : 

		PlaneCollider* pPlane;
		pPlane = (PlaneCollider*)pCollider;

		//反射ベクトルを算出
		reflection = physics::CalcReflection(velocity_, pPlane->GetNormal());
	
		force_ += reflection;

		break;

	case AABB:  break;

	case OBB: 
		
		break;
	}

}

void Bullet::SetMoveDir(D3DXVECTOR3 moveDir)
 {
	D3DXVec3Normalize(&moveDir, &moveDir);
	moveDir_ = moveDir;
}


	

