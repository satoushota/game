#include "Ground.h"
#include "Engine/Model.h"
#include "Engine/PlaneCollider.h"

//コンストラクタ
Ground::Ground(IGameObject * parent)
	:IGameObject(parent, "Ground"),hModel_(-1)
{
}

//デストラクタ
Ground::~Ground()
{
}

//初期化
void Ground::Initialize()
{
	//モデルの読み込み
	hModel_ = Model::Load("Data/model/Play_Ground.fbx");
	assert(hModel_ != -1);

	//平面の当たり判定を作成
	PlaneCollider * collider = new PlaneCollider(this, position_, D3DXVECTOR3(0, 1, 0));
	SetCollider(collider);

}

//更新
void Ground::Update()
{

}

//描画
void Ground::Draw()
{
	//モデルの描画
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Ground::Release()
{
}
