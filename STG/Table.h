#pragma once
#include "Engine/Global.h"

//◆◆◆を管理するクラス
class Table : public IGameObject
{
	//モデル番号
	int hModel_;

public:
	//コンストラクタ
	Table(IGameObject* parent);

	//デストラクタ
	~Table();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//衝突応答
	void OnCollision(IGameObject* pTarget, Collider* pMyCollider,Collider* pCollider) override;
};