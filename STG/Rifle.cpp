#include "Rifle.h"
#include "Engine/Model.h"
#include "Engine/Image.h"
#include "PlayScene.h"
#include "Engine/Audio.h"
//コンストラクタ
Rifle::Rifle(IGameObject * parent)
	:IGameObject(parent, "Rifle"),hImage_(-1),bulletCount_(0) ,muzzleDir_(D3DXVECTOR3(0,0,0)),bulletMove_(D3DXVECTOR3(0, 0, 0))
{
}

//デストラクタ
Rifle::~Rifle()
{
}

//初期化
void Rifle::Initialize()
{
	//照準画像の読み込み
	hImage_ = Image::Load("Data/Image/Play_Reticle.png");
	assert(hImage_ != -1);
	
	//銃口の位置をセット
	muzzleDir_ = D3DXVECTOR3(position_.x, position_.y + 0.15f, position_.z + 1.0f);

	//弾をセット
	bulletCount_ = 3;
}

//更新
void Rifle::Update()
{
	//弾の飛ぶ方向を作成
	CreateBulletMove();
	
	//弾を発射
	if (Input::IsKeyUp(DIK_SPACE) && bulletCount_ != 0)
	{
		Audio::Play("Shot");
		//弾を生成して弾の初期位置を銃の先に設置
 		pBullet_ = CreateGameObject<Bullet>(pParent_->GetParent());
		pBullet_->SetPosition(pParent_->GetPosition());

		//弾を消費
		bulletCount_--;
	}

	//残段数がゼロになったらシーンを切り替えてもらう
	if (bulletCount_ == 0)
	{
		//親の親であるプレイシーンの情報を取得
		IGameObject* scene = pParent_->GetParent();

		//シーンを切り替えてもらう
		((PlayScene*)scene)->ChangeScene();
	}
}

void Rifle::CreateBulletMove()
{
	//弾の飛ぶ方向を計算
	bulletMove_ = muzzleDir_ - position_;

	//移動回転行列(x,y,z)、拡大行列
	D3DXMATRIX matRX, matRY, matRZ, matRXYZ;

	//飛ぶ方向をプレイヤーが向いてる方向で回転させる
	D3DXMatrixRotationX(&matRX, D3DXToRadian(pParent_->GetRotate().x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(pParent_->GetRotate().y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(pParent_->GetRotate().z));

	matRXYZ = matRX * matRY * matRZ;

	D3DXVec3TransformCoord(&bulletMove_, &bulletMove_, &matRXYZ);
}

//描画
void Rifle::Draw()
{
	Image::SetMatrix(hImage_, localMatrix_);
	Image::Draw(hImage_);
}

//開放
void Rifle::Release()
{
}

D3DXVECTOR3 Rifle::GetBulletMove_()
{
	D3DXVec3Normalize(&bulletMove_, &bulletMove_);
	return bulletMove_;
}
