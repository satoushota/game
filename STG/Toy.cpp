#include "Toy.h"
#include "Engine/Model.h"
#include "Engine/OBBCollider.h"
#include "Score.h"

//コンストラクタ
Toy::Toy(IGameObject * parent)
	:IGameObject(parent, "Toy"),hModel_(-1)
{
}

//デストラクタ
Toy::~Toy()
{
}

//初期化
void Toy::Initialize()
{
	hModel_ = Model::Load("Data/model/Play_redBox.fbx");
	assert(hModel_ != -1);
	
	OBBCollider* pOBB = new OBBCollider(this,D3DXVECTOR3(1,2,1), position_);
	
	SetCollider(pOBB);
}

//更新
void Toy::Update()
{
}

//描画
void Toy::Draw()
{
	Model::SetMatrix(hModel_, worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Toy::Release()
{
}

void Toy::OnCollision(IGameObject * pTarget, Collider * pMyCollider, Collider * pCollider)
{
	//親の子供一覧をもらう
	auto i = pParent_->GetChildList()->begin();
	
	//子の中からスコアを見つけだしスコアを加算
	while (true)
	{
		if ((*i)->GetObjectName() == "Score")
		{
			IGameObject* score = (*i);
			((Score*)score)->AddScore(250);
			break;
		}
		i++;
	}

	
	KillMe();
}
