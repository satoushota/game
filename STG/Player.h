#pragma once
#include "Engine/Global.h"
#include "Rifle.h"

class Camera;
class Rifle;

//自機を管理するクラス
class Player : public IGameObject
{
	//カメラを管理するためのポインタ
	Camera* pCamera_;

	//銃を管理するためのポインタ
	Rifle* pRifle_;


	//移動の処理をまとめた関数
	//プレイヤー以外が使うことはないのでprivateおいておく
	void Move();

public:
	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};