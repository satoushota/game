#include "ResultScene.h"
#include "Engine/SceneManager.h"
#include "Engine/Audio.h"
#include "Score.h"

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent)
    : IGameObject(parent, "ResultScene"),countStop_(true)
{

}

//初期化
void ResultScene::Initialize()
{
	pScore_ = CreateGameObject<Score>(this);

	//BGMの再生
	Audio::Play("Score_BGM");
}

//更新
void ResultScene::Update()
{
	time_ += 0.1;

	//スペースキーを押したらシーン移動
	if (Input::IsKeyUp(DIK_SPACE))
	{
		//演出をカット
		countStop_ = false;

		Audio::Stop("Score_BGM");
		
		//スコアとハイスコアどっちも表示されていたらシーン移動
		if (pScore_->GetScoreShowFlag() && pScore_->GetHighScoreShowFlag())
		{
			SceneManager::ChangeScene(SCENE_ID_TITLE);
		}

		//ハイスコア表示（スコアを表示していてスペースが押されたら）
		if (pScore_->GetScoreShowFlag() && !pScore_->GetHighScoreShowFlag())
		{
			Audio::Play("Highscore_Show");
			((Score*)pScore_)->SetHighScoreShowFlag(true);

			//ハイスコアを更新していたら
			if (pScore_->GetHighScoreUpdateFlag())
			{
				Audio::Play("Highscore_Update");
				pScore_->SetHighScoreUpdateShowFlag(true);
			}
		}
		
		//スコアを表示していなかったらスコアを表示
		if (!pScore_->GetScoreShowFlag())
		{
			Audio::Play("Score_Show");
			((Score*)pScore_)->SetScoreShowFlag(true);
		}
		
	}

}

//描画
void ResultScene::Draw()
{
	//スキップされていないならドラムロールとともにスコアを表示
	if (countStop_)
	{
		//6秒たったらスコアを表示
		if (time_ > 36)
		{
			//スコアを表示したら鳴らす
			if (time_ < 36.1)
			{
				Audio::Stop("Score_BGM");
				Audio::Play("Score_Show");
			}

			((Score*)pScore_)->SetScoreShowFlag(true);
		}

		//8秒たったらハイスコアを表示
		if (time_ > 48)
		{
			//ハイスコアを表示したら音を鳴らす
			if (time_ < 48.1)
			{
				Audio::Play("Highscore_Show");
			}

			((Score*)pScore_)->SetHighScoreShowFlag(true);
		}
	}
	
}

//開放
void ResultScene::Release()
{
}