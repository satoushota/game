#pragma once
#include "Engine/Global.h"

const D3DXVECTOR3 GRAVITY_ACCELERATION = D3DXVECTOR3(0, M_S(-9.8f), 0);

//弾を管理するクラス
class Bullet : public IGameObject
{
	//モデル番号
	int hModel_;
	
	//飛ぶ方向
	D3DXVECTOR3 moveDir_;

	//加わる力の合計
	D3DXVECTOR3 force_;

	//加速度
	D3DXVECTOR3 acceleration_;

	//速度
	D3DXVECTOR3 velocity_;

	//質量
	float mass_;

public:
	//コンストラクタ
	Bullet(IGameObject* parent);

	//デストラクタ
	~Bullet();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//衝突処理
	void OnCollision(IGameObject* pTarget, Collider* pMyCollider, Collider* pCollider) override;

	//弾の飛ぶ方向を決める際に使用（セッター）
	void SetMoveDir(D3DXVECTOR3 moveDir);

};
