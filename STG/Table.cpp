#include "Table.h"
#include "Engine/Model.h"
#include "Engine/AABBCollider.h"
#include "Engine/OBBCollider.h"

//コンストラクタ
Table::Table(IGameObject * parent)
	:IGameObject(parent, "Table")
{
}

//デストラクタ
Table::~Table()
{
}

//初期化
void Table::Initialize()
{
	hModel_ = Model::Load("Data/model/Play_Table.fbx");
	
	position_.y = 0;

	OBBCollider* collider = new OBBCollider(this, D3DXVECTOR3(5, 1, 1.8), D3DXVECTOR3(0,0.5,0));
	SetCollider(collider);
}

//更新
void Table::Update()
{

}

//描画
void Table::Draw()
{
	Model::SetMatrix(hModel_,worldMatrix_);
	Model::Draw(hModel_);
}

//開放
void Table::Release()
{

}

void Table::OnCollision(IGameObject * pTarget, Collider* pMyCollider,Collider * pCollider)
{
	
}
