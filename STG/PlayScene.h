#pragma once
#include"Engine/Global.h"
#include "Bullet.h"
#include "Ground.h"
#include "Table.h"
#include "Player.h"
#include "Rack.h"
#include "Score.h"

//前方宣言
class Bullet;
class Ground;
class Player;
class Rack;
class Score;


//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	//プレイヤー
	Player* pPlayer_;

	//弾
	Bullet* pBullet_;

	//地面
	Ground* pGround_;
	
	//台
	Table* pTable_;
	
	//商品棚
	Rack* pRack_;

	//スコア
	Score* pScore_;

	//シーンを切り替えるかどうか
	bool changeScene_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//リザルトシーンに移行する
	void ChangeScene();
};