#include "OBBCollider.h"

OBBCollider::OBBCollider(IGameObject* owner, D3DXVECTOR3 boxSize, D3DXVECTOR3 center)
{
	//持ち主の登録
	owner_ = owner;

	//OBBの大きさ
	boxSize_ = boxSize;

	//OBBの各辺の半分の長さ
	boxHalfSize_ = D3DXVECTOR3(boxSize_.x / 2.0f, boxSize_.y / 2.0f, boxSize_.z  / 2.0f);

	//OBBの中心
	center_ = center;

	shape_ = OBB;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateBox(Direct3D::pDevice, boxSize_.x, boxSize_.y, boxSize_.z, &pMesh_, 0);
#endif
}


OBBCollider::~OBBCollider()
{
}

bool OBBCollider::IsHit(Collider * pTarget)
{
	
	switch (pTarget->shape_)
	{
	case SPHERE:return IsHitOBBAndSphere(this, (SphereCollider*)pTarget);

	}

	return false;
}

