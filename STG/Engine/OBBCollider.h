#pragma once
#include "Collider.h"

class OBBCollider: public Collider
{
	friend class Collider;

	//箱の各辺の大きさ
	D3DXVECTOR3 boxSize_;

	//箱の各辺の半分の大きさ
	D3DXVECTOR3 boxHalfSize_;

public:
	OBBCollider(IGameObject* owner,  D3DXVECTOR3 boxSize, D3DXVECTOR3 center);
	~OBBCollider();

	bool IsHit(Collider* pTarget) override;
};

