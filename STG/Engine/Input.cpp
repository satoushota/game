#include<assert.h>
#include "Input.h"

namespace Input
{
	//外部でも使う場合はｈに書くが今回はここでしか使わないのでcppに書いた
	LPDIRECTINPUT8   pDInput = nullptr;
	//ここでいうデバイスはキーボード
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;
	//unsigned char きーが
	BYTE keyState[256] = { 0 };
	//前フレームでの各キーの状態
	BYTE prevKeyState[256] = { 0 };

	//マウス
	LPDIRECTINPUTDEVICE8	pMouseDevice;	//デバイスオブジェクト
	DIMOUSESTATE mouseState;				//マウスの状態
	DIMOUSESTATE prevMouseState;			//前フレームのマウスの状態

	//ウィンドウハンドル
	HWND hWnd_;

	void Initialize(HWND hWnd)
	{
		//ダイレクトインプットの準備準備
		DirectInput8Create(GetModuleHandle(nullptr), DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&pDInput, nullptr);
		assert(pDInput != nullptr);

		//1行目でデバイスオブジェクトを作成
		pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);
		assert(pKeyDevice != nullptr);

		//2行目でデバイスの種類（今回はキーボード）を指定
		pKeyDevice->SetDataFormat(&c_dfDIKeyboard);

		//3行目で強調レベル（他の実行中のアプリに対する優先度）の設定 をしている。	
		pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);	


		//マウス
		pDInput->CreateDevice(GUID_SysMouse, &pMouseDevice, nullptr);
		pMouseDevice->SetDataFormat(&c_dfDIMouse);
		pMouseDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);


		hWnd_ = hWnd;
	}

	void Update()
	{
		memcpy(prevKeyState,keyState,sizeof(keyState));

		//キーボードを見失ったときに見つける（仕様）
		pKeyDevice->Acquire();
		//ここが実行された時の情報を入れる
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);

		//マウス
		pMouseDevice->Acquire();
		memcpy(&prevMouseState, &mouseState, sizeof(mouseState));
		pMouseDevice->GetDeviceState(sizeof(mouseState), &mouseState);
	}

	bool IsKey(int keyCode)
	{
		//右から８ビット目が１に立ってるかを調べるために１２８をかける
		if (keyState[keyCode] & 0x80 )
		{
			return true;
		}

		return false;
	}

	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		if (IsKey(keyCode) && (prevKeyState[keyCode] & 0x80) == 0)
		{
			return true;
		}

		return false;
	}

	bool IsKeyUp(int keyCode)
	{
		//さっきは押してて、今は押してない状態
		if (!IsKey(keyCode) && prevKeyState[keyCode] & 0x80)
		{
			return true;
		}

		return false;
	}

	/////////////////////////////　マウス情報取得　//////////////////////////////////

	//マウスのボタンが押されているか調べる
	bool IsMouseButton(int buttonCode)
	{
		//押してる
		if (mouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		if (IsMouseButton(buttonCode) && !(prevMouseState.rgbButtons[buttonCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今放したか調べる
	bool IsMouseButtonUp(int buttonCode)
	{
		//今押してなくて、前回は押してる
		if (!IsMouseButton(buttonCode) && prevMouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	D3DXVECTOR3 GetMousePosition()
	{
		POINT mousePos;
		GetCursorPos(&mousePos);
		//マウスの場所はウィンドウの場所から変換
		ScreenToClient(hWnd_, &mousePos);

		D3DXVECTOR3 result = D3DXVECTOR3(-mousePos.y, mousePos.x, 0.0f);

		return result;
	}

	D3DXVECTOR3 GetMouseMove()
	{
		D3DXVECTOR3 result(mouseState.lX, mouseState.lY, mouseState.lZ);
		return result;
		
	}

	void Release()
	{
		SAFE_RELEASE(pMouseDevice);
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}
}