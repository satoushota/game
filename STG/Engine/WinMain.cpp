#include <Windows.h>
#include "Global.h"
#include "Direct3D.h"
#include "RootJob.h"
#include "Model.h"
#include "Image.h"
#include "Audio.h"


//メモリーリーク検出用
#ifdef _DEBUG
#include <crtdbg.h>
#endif

//リンカ
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")

//グローバル変数
const char* WIN_CLASS_NAME = "SampleGame";
const int	WINDOW_WIDTH = 800;		//ウィンドウの幅
const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ

//グローバル変数
Global g;				//画面サイズなどの情報
RootJob *pRootJob;		//すべてゲームのオブジェクトのもととなるクラス

//プロトタイプ宣言
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);		//ウィンドウプロシージャ

//エントリーポイント　
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	//メモリリーク検出
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	//ウィンドウクラス（設計図）を作成
	WNDCLASSEX wc;

	wc.cbSize = sizeof(WNDCLASSEX);								//構造体(WNDCLASSEX)のサイズ
	wc.hInstance = hInstance;									//インスタンスハンドル
	wc.lpszClassName = WIN_CLASS_NAME;							//ウィンドウクラス名		
	wc.lpfnWndProc = WndProc;									//ウィンドウプロシージャ
	wc.style = CS_VREDRAW | CS_HREDRAW;							//スタイル（デフォルト
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);					//アイコン
	wc.hIconSm = LoadIcon(NULL, IDI_WINLOGO);					//小さいアイコン
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);					//マウスカーソル
	wc.lpszMenuName = NULL;										//メニュー
	wc.cbClsExtra = 0;											//構造体に追加で割り当てるバイト数
	wc.cbWndExtra = 0;											//ウィンドウインスタンスに追加で割り当てるバイト数
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);		//背景

	RegisterClassEx(&wc);										//ウィンドウクラスを登録

	RECT winRect{ 0,0,WINDOW_WIDTH,WINDOW_HEIGHT };				//ウィンドウの幅を登録
	AdjustWindowRect(&winRect, WS_OVERLAPPEDWINDOW, FALSE);		//ウィンドウのサイズを決定

	//ウィンドウを作成
	HWND hWnd = CreateWindow(			
		WIN_CLASS_NAME,					//ウィンドウクラス
		"PHYSICS SHOOTER", 				//タイトルバーに表示する内容
		WS_OVERLAPPEDWINDOW,			//スタイル
		CW_USEDEFAULT,					//表示位置左
		CW_USEDEFAULT,					//表示位置上
		winRect.right - winRect.left,	//ウィンドウ幅  
		winRect.bottom - winRect.top,	//ウィンドウ高さ
		NULL,							//親ウインドウ
		NULL,							//メニュー
		hInstance,						//インスタンス
		NULL							//パラメータ（なし）
	);
	assert(hWnd != NULL);

	//ウィンドウを表示
	ShowWindow(hWnd, nCmdShow);		

	//ShowCursor(false);
	
	//ウィンドウの幅を登録
	g.screenWidth = WINDOW_WIDTH;
	g.screenHeight = WINDOW_HEIGHT;

	//初期化
	Direct3D::Initialize(hWnd);			//DirecXの準備

	Input::Initialize(hWnd);			//入力処理の準備

	Audio::Initialize();				//オーディオの準備
	Audio::WaveBankLoad("Data/Sound/WaveBank.xwb");
	Audio::SoundBankLoad("Data/Sound/SoundBank.xsb");

	pRootJob = new RootJob;				//ゲームオブジェクトの準備
	pRootJob->Initialize();

	MSG msg;							//メッセージ受け取りの準備
	ZeroMemory(&msg, sizeof(msg));


	//メッセージループ
	while (msg.message != WM_QUIT)
	{
		//メッセージあり
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		//メッセージなし
		else
		{
			//入力情報の更新
			Input::Update();

			//ゲームの更新
			pRootJob->UpdateSub();

			//描画開始
			Direct3D::BeginDraw();
			pRootJob->DrawSub();

			//描画終了
			Direct3D::EndDraw();
		}
	}



	//開放処理
	pRootJob->ReleaseSub();
	SAFE_DELETE(pRootJob);
	Audio::Release();
	Model::AllRelase();
	Image::AllRelase();
	Input::Release();	
	
	Direct3D::Release();

	ShowCursor(true);

	return 0;
}


//ウィンドウプロシージャ
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:			//ウィンドウの×が押されたら〜
		PostQuitMessage(0);	
		return 0;
	}

	return DefWindowProc(hWnd, msg, wParam, lParam);
}