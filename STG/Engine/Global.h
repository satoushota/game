#pragma once
#include <d3dx9.h>
#include<assert.h>
#include"Input.h"
#include"IGameObject.h"

//fps数
const int FPS = 60;

//60FPS用時間マクロ
#define SECOND(sec)	((sec)*FPS)				//秒
#define MINUTE(min) (SECOND((min)*FPS))		//分
#define HOUR(hour)	((MINUTE(hour)*FPS))	//時

//M(メートル)基準距離マクロ
#define M(m)	(m)					//メートル
#define MM(mm)	(M(0.001f*(mm)))	//ミリメートル
#define CM(cm)	(M(0.01f*(cm)))		//センチメートル
#define KM(km)	(M(1000.0f)*(km))	//キロメートル

//速度マクロ
#define M_S(speed)	(M(speed) / SECOND(1.0f))	//秒速
#define KM_H(speed) (KM(speed) / HOUR(1.0f))	//時速

//開放マクロ
#define SAFE_DELETE(p)			if(p != nullptr){ delete p; p = nullptr;}
#define SAFE_DELETE_ARRAY(p)	if(p != nullptr){ delete[] p; p = nullptr;}
#define SAFE_RELEASE(p)			if(p != nullptr){ p->Release(); p = nullptr;}

struct Global
{
	int screenWidth;	//ウィンドウの幅
	int screenHeight;	//ウィンドウの横幅
};

extern Global g;


//extern const int	WINDOW_WIDTH = 800;	 //ウィンドウの幅
//extern const int	WINDOW_HEIGHT = 600;	//ウィンドウの高さ
