#include "Collider.h"
#include "Direct3D.h"
#include "SphereCollider.h"
#include "PlaneCollider.h"
#include "AABBCollider.h"
#include "OBBCollider.h"

Collider::Collider()
{
}


Collider::~Collider()
{
}

bool Collider::IsHitSphereAndSphere(SphereCollider * sphere1, SphereCollider * sphere2)
{
	//自分の球の中心と相手の球の中心との距離を取得（ベクトル）
	D3DXVECTOR3 vecLength = (sphere1->center_ + sphere1->owner_->GetPosition()) 
		- (sphere2->center_ + sphere2->owner_->GetPosition());
	
	//↑求めたベクトルを距離に変換
	float length = D3DXVec3Length(&vecLength);

	//二つの球との距離が二つの球の半径の合計より小さいなら真
	if (length <= (sphere1->radius_ + sphere2->radius_))
	{

		return true;
	}

	return false;
}

bool Collider::IsHitPlaneAndSphere(PlaneCollider * plane, SphereCollider * sphere)
{

	//球の位置
	D3DXVECTOR3 spherePos = sphere->center_ + sphere->owner_->GetPosition();

	//平面上の点
	D3DXVECTOR3 planePoint = (D3DXVECTOR3)plane->plane_;

	//平面の法線
	D3DXVECTOR3 planeNormal = plane->normal_;

	//球の半径
	float radius = sphere->radius_;

	//平面上の点から球の中心に伸びるベクトル
	D3DXVECTOR3 vec = spherePos - planePoint;

	//球の進行方向ベクトル
	D3DXVECTOR3 sphereMoveDir = spherePos - sphere->prePos_;

	//平面と球の中心の距離
	float planeSpherLength = D3DXVec3Dot(&vec, &plane->normal_);
	float planeSpherLengthAbs = fabs(planeSpherLength);

	//進行方向ベクトルと平面の内積（平面に向かって進んでいるかの判断）
	float dot = D3DXVec3Dot(&vec, &planeNormal);
	
	//衝突時間
	float time;

	//壁とめり込んでいるかを調べる
	if ((0.00001f - fabs(dot) > 0.0f) && (planeSpherLengthAbs < radius))
	{	
		return true;
	}

	//衝突時間の計算
	time = (radius - planeSpherLengthAbs) / dot;

	//衝突位置（前回の位置 +  衝突ベクトル(衝突時間　* 進行ベクトル)）
	D3DXVECTOR3 collisionPos = sphere->prePos_ + (time * sphereMoveDir);

	sphere->SetPrePos(spherePos);

	//進行方向が壁に対して逆だったらぶつからない
	if (dot >= 0)
	{
		return false;
	}

	//衝突時間が０〜１の間だったら衝突
	if ((0 <= time) && (time <= 1))
	{
		
		return true;
	}


	return false;
}

bool Collider::IsHitAABBAndSphere(AABBCollider * box, SphereCollider * sphere)
{
	//球の中心位置
	D3DXVECTOR3 spherePos = sphere->center_ + sphere->owner_->GetPosition();

	//AABBの最小位置
	D3DXVECTOR3 boxPosMin = box->boxMin_ + box->owner_->GetPosition();

	//AABBの最大位置
	D3DXVECTOR3 boxPosMax = box->boxMax_ + box->owner_->GetPosition();
	
	//球の中心からAABBへの最短距離べき乗
	float length = 0;

	//AABBの各軸の最短距離を算出
	//球の中心がAABBのよりも左にあった場合
	if (spherePos.x < boxPosMin.x)
	{
		length += (spherePos.x - boxPosMin.x) * (spherePos.x - boxPosMin.x);
	}
	//球の中心がAABBよりも右にあった場合
	if (spherePos.x > boxPosMax.x)
	{
		length += (spherePos.x - boxPosMax.x) * (spherePos.x - boxPosMax.x);
	}
	//球の中心がAABBよりも下にあった場合
	if (spherePos.y < box->boxMin_.y)
	{
		length += (spherePos.y - boxPosMin.y) * (spherePos.y - boxPosMin.y);
	}
	//球の中心がAABBよりも上にあった場合
	if (spherePos.y > boxPosMax.y)
	{
		length += (spherePos.y - boxPosMax.y) * (spherePos.y - boxPosMax.y);
	}
	//球の中心がAABBよりも手前にあった場合
	if (spherePos.z < boxPosMin.z)
	{
		length += (spherePos.z - boxPosMin.z) * (spherePos.z - boxPosMin.z);
	}
	//球の中心がAABBよりも奥にあった場合
	if (spherePos.z > boxPosMax.z)
	{
		length += (spherePos.z - boxPosMax.z) * (spherePos.z - boxPosMax.z);
	}

	//距離が０だったらもうAABBの中に球が入っているので衝突
	//算出した距離（べき乗）を平方根に直してその距離が球の半径よりも短いなら衝突している
	if (length == 0 || (sqrtf(length) <= sphere->radius_))
	{
		return true;
	}

	return false;
}

bool Collider::IsHitOBBAndSphere(OBBCollider * box, SphereCollider * sphere)
{
	//最短距離
	D3DXVECTOR3 vecLength = D3DXVECTOR3(0,0,0);

	//球の中心
	D3DXVECTOR3 spherePos = sphere->center_ + sphere->owner_->GetPosition();

	//oBBの中心
	D3DXVECTOR3 boxPos = box->center_ + box->owner_->GetPosition();

	////// X //////

	//x方向の最短距離を求める(半分の長さ)
	float length = box->boxHalfSize_.x;

	//はみ出し部分を求めるための係数を算出
	float s = D3DXVec3Dot(&(spherePos - boxPos), &D3DXVECTOR3(1,0,0)) / length;

	//マイナス値が含まれるので絶対値に変更
	s = fabs(s);

	//はみ出し部分の算出
	//係数が１をこえていると各辺の外に飛び出しているので計算
	if (s > 1)
	{
		vecLength += (1 - s) * length * D3DXVECTOR3(1, 0, 0);
	}

	///// Y /////
	
	//y方向の最短距離を求める(半分の長さ)
	length = box->boxHalfSize_.y;

	//はみ出し部分を求めるための係数を算出
	 s = D3DXVec3Dot(&(spherePos - boxPos), &D3DXVECTOR3(0, 1, 0)) / length;

	//マイナス値が含まれるので絶対値に変更
	s = fabs(s);

	//はみ出し部分の算出
	//係数が１をこえていると各辺の外に飛び出しているので計算
	if (s > 1)
	{
		vecLength += (1 - s) * length * D3DXVECTOR3(0, 1, 0);
	}

	///// z /////

	//z方向の最短距離を求める(半分の長さ)
	length = box->boxHalfSize_.z;

	//はみ出し部分を求めるための係数を算出
	s = D3DXVec3Dot(&(spherePos - boxPos), &D3DXVECTOR3(0, 0, 1)) / length;

	//マイナス値が含まれるので絶対値に変更
	s = fabs(s);

	//はみ出し部分の算出
	//係数が１をこえていると各辺の外に飛び出しているので計算
	if (s > 1)
	{
		vecLength += (1 - s) * length * D3DXVECTOR3(0, 0, 1);
	}

	//球の中心からOBBへの最短距離が球の半径より短いなら当たっている
	if (sphere->radius_ > D3DXVec3Length(&vecLength))
	{
	  D3DXVECTOR3 a = boxPos - spherePos;

		return true;
	}

	return false;
}

void Collider::Draw(D3DXVECTOR3 position)
{
	D3DXMATRIX mat;
	D3DXMatrixTranslation(&mat, position.x + center_.x, position.y + center_.y, position.z + center_.z);
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &mat);
	pMesh_->DrawSubset(0);
}

ColliderShape Collider::GetShape()
{
	return shape_;
}



