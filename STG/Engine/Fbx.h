#pragma once
#include "Global.h"
#include<fbxsdk.h>

#pragma comment(lib,"libfbxsdk-mt.lib")

class Fbx
{
protected:

	int vertexCount_;	//頂点数
	
	int polygonCount_;	//ポリゴン数
   
	int indexCount_;
	
	int materialCount_;
	
	int* polygonCountOfMaterial_;
	
	struct Vertex
	{
		//ポジション	
		D3DXVECTOR3 pos;
		D3DXVECTOR3 normal;
		D3DXVECTOR2 uv;
	};

	FbxManager*  pManager_;
	FbxImporter* pImporter_;
	FbxScene*    pScene_;
	D3DMATERIAL9*        pMaterial_;

	Vertex vertexList;

	//頂点情報
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;
	LPDIRECT3DINDEXBUFFER9* ppIndexBuffer_;


	LPDIRECT3DTEXTURE9* pTexture_;

	//D3DMATERIAL9         material_;

	//ノードを調べる
	void CheckNode(FbxNode* pNode);

	void CheckMesh(FbxMesh* pMesh);
public:
	Fbx();
	~Fbx();

	void Load(const char* fileName);
	void Draw(const D3DXMATRIX& matrix);
};
