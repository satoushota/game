#include "SphereCollider.h"



SphereCollider::SphereCollider(IGameObject* owner, D3DXVECTOR3 center, float radius)
{
	owner_ = owner;

	center_ = center;

	prePos_ = (center_ + owner_->GetPosition());

	radius_ = radius;

	shape_ = SPHERE;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateSphere(Direct3D::pDevice, radius, 8, 4, &pMesh_, 0);
#endif
}


SphereCollider::~SphereCollider()
{
}

bool SphereCollider::IsHit(Collider * pTarget)
{
	switch (pTarget->shape_)
	{
	case SPHERE: return IsHitSphereAndSphere(this,(SphereCollider*)pTarget);
	case PLANE:  return IsHitPlaneAndSphere((PlaneCollider*)pTarget, this);
	case AABB:	 return IsHitAABBAndSphere((AABBCollider*)pTarget, this);
	case OBB:    return IsHitOBBAndSphere((OBBCollider*)pTarget, this);
	}

	return false;
}

void SphereCollider::SetPrePos(D3DXVECTOR3 prepos)
{
	prePos_ = prepos;
}

