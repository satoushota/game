#include "Fbx.h"
#include "Direct3D.h"




Fbx::Fbx()
	:pVertexBuffer_(nullptr), ppIndexBuffer_(nullptr), pMaterial_(nullptr),pTexture_(nullptr), vertexCount_(0), polygonCount_(0),indexCount_(0), materialCount_(0),
	pManager_(nullptr), pImporter_(nullptr), pScene_(nullptr)
{
}


Fbx::~Fbx()
{
	//作った準とは逆に開放していく
	//SAFE_RELEASE(pTexture_);
	SAFE_DELETE_ARRAY(polygonCountOfMaterial_);
	SAFE_DELETE_ARRAY(pMaterial_);

	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(ppIndexBuffer_[i]);
		SAFE_RELEASE(pTexture_[i]);
	}

	SAFE_DELETE_ARRAY(ppIndexBuffer_);
	SAFE_DELETE_ARRAY(pTexture_);

	SAFE_RELEASE(pVertexBuffer_);
	pScene_->Destroy();
	pManager_->Destroy();
}

void Fbx::Load(const char * fileName)
{
	//マネージャーを作り開く準備をする
	pManager_ = FbxManager::Create();

	//開くためのインポーターを用意
	pImporter_ = FbxImporter::Create(pManager_, "");
	pScene_ = FbxScene::Create(pManager_, "");

	//ファイルを開く
	pImporter_->Initialize(fileName);
	pImporter_->Import(pScene_);

	//開いたので役目終了
	pImporter_->Destroy();

	//この後カレントディレクトリを変更するのでここでデフォルトのカレントディレクトリを取得
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//フォルダ名を取得
	char dir[MAX_PATH];
	_splitpath_s(fileName, nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	//この関数で探す場所を変更できる
	SetCurrentDirectory(dir);

	//ルートノードと子供の数を取得
	FbxNode* rootNode = pScene_->GetRootNode();
	int childCount = rootNode->GetChildCount();
	//子供の数分ループ
	for (int i = 0; childCount > i; i++)
	{
		//ノードの内容をチェック
		CheckNode(rootNode->GetChild(i));
	}

	//カレントディレクトリをデフォルトに変更
	SetCurrentDirectory(defaultCurrentDir);

}

void Fbx::CheckNode(FbxNode * pNode)
{
	//Fbx情報取得
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();

	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//メッシュノードだった
		materialCount_ = pNode->GetMaterialCount();
		pMaterial_ = new D3DMATERIAL9[materialCount_];
		pTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];

		//ダブルポインタなのでここで初期化
		ZeroMemory(pTexture_, sizeof(LPDIRECT3DTEXTURE9) * materialCount_);

		for (int i = 0; i < materialCount_; i++)
		{
			//ランバートモデル読み込みテカリとかがあると無理らしい
			FbxSurfaceLambert* lambert = (FbxSurfaceLambert*)pNode->GetMaterial(i);

			//拡散反射光（そのものの色）と環境光を会得
			FbxDouble3 diffuse = lambert->Diffuse;
			FbxDouble3 ambient = lambert->Ambient;

			ZeroMemory(&pMaterial_[i], sizeof(D3DMATERIAL9));

			pMaterial_[i].Diffuse.r = (float)diffuse[0];
			pMaterial_[i].Diffuse.g = (float)diffuse[1];
			pMaterial_[i].Diffuse.b = (float)diffuse[2];
			pMaterial_[i].Diffuse.a = 1.0f;

			pMaterial_[i].Ambient.r = (float)ambient[0];
			pMaterial_[i].Ambient.g = (float)ambient[1];
			pMaterial_[i].Ambient.b = (float)ambient[2];
			pMaterial_[i].Ambient.a = 1.0f;

			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			//テクスチャが入っていなかったとき
			if (textureFile == nullptr)
			{
				pTexture_[i] = nullptr;
			}
			else
			{
				const char* textureFileName = textureFile->GetFileName();
				char name[_MAX_FNAME];
				char ext[_MAX_EXT];
				_splitpath_s(textureFileName, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
				wsprintf(name, "%s%s", name, ext);
				
				D3DXCreateTextureFromFileEx(Direct3D::pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN,
					D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, 0, 0, 0, &pTexture_[i]);
				//cdcdcdcdはおかしいからね
				assert(pTexture_[i] != nullptr);
			}
		}

		CheckMesh(pNode->GetMesh());
		
	}
	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			CheckNode(pNode->GetChild(i));
		}
	}
}

void Fbx::CheckMesh(FbxMesh* pMesh)
{
	FbxVector4* pVertexPos = pMesh->GetControlPoints();
	//頂点数記録	
	vertexCount_ = pMesh->GetControlPointsCount();
	//配列にしてそのおおきさ分確保
	Vertex* vertexList = new Vertex[vertexCount_];

	polygonCount_ = pMesh->GetPolygonCount();
	indexCount_ = pMesh->GetPolygonVertexCount();

	for (int i = 0; vertexCount_ > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}

	//法線
	for (int i = 0; i < polygonCount_; i++)
	{
		//ポリゴンの始まり頂点の習得
		int startIndex = pMesh->GetPolygonVertexIndex(i);
		for (int j = 0; j < 3; j++)
		{
			//上と他二つを会得
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			//法線情報の登録
			FbxVector4 Normal;
			pMesh->GetPolygonVertexNormal(i, j, Normal);
			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);

			//UV情報の登録
			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}

	Direct3D::pDevice->CreateVertexBuffer(sizeof(Vertex) *vertexCount_, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&pVertexBuffer_, 0);
	assert(&pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex) *vertexCount_);
	pVertexBuffer_->Unlock();

	SAFE_DELETE_ARRAY(vertexList);
	
	ppIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];
	polygonCountOfMaterial_ = new int[materialCount_];

	for (int i = 0; i < materialCount_; i++)
	{
		int* indexList = new int[indexCount_];
		int count = 0;

		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			//ポリゴンが何番のマテリアルのつかっているかを判定特定のいろだったら反応しそれぞれの色ごとにマテリアルを作成
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);
			if (materialID == i)
			{
				//頂点番号登録
				for (int vertex = 0; vertex < 3; vertex++)
				{
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}

		//一ポリゴンに３頂点必要だからマテリアルごとのポリゴンがわかる
		polygonCountOfMaterial_[i] = count / 3;

		Direct3D::pDevice->CreateIndexBuffer(sizeof(int) * indexCount_, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &ppIndexBuffer_[i], 0);
		assert(ppIndexBuffer_ != nullptr);

		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int) * indexCount_);
		ppIndexBuffer_[i]->Unlock();

		SAFE_DELETE_ARRAY(indexList);
	}

}

void Fbx::Draw(const D3DXMATRIX &matrix)
{
	//引数は表示したい場所が入ってる
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));

	for (int i = 0; i < materialCount_; i++)
	{
		Direct3D::pDevice->SetTexture(0, pTexture_[i]);
		Direct3D::pDevice->SetIndices(ppIndexBuffer_[i]);
		Direct3D::pDevice->SetMaterial(&pMaterial_[i]);
		Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, polygonCountOfMaterial_[i]);
	}
}

