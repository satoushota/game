#include "Direct3D.h"

//初期化
LPDIRECT3D9  Direct3D::pD3d = nullptr;
LPDIRECT3DDEVICE9	Direct3D::pDevice = nullptr;

void Direct3D::Initialize(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);
	assert(pD3d != nullptr);

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;					//専用の構造体を作成
	ZeroMemory(&d3dpp, sizeof(d3dpp));
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.screenWidth;
	d3dpp.BackBufferHeight = g.screenHeight;
	pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice);
	assert(pDevice != nullptr);


	//アルファブレンド
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//ライティング
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	//ライト
	D3DLIGHT9 lightState;							//ライト情報を生成
	ZeroMemory(&lightState, sizeof(lightState));

	
	lightState.Type = D3DLIGHT_DIRECTIONAL;			//光源の種類
	lightState.Direction = D3DXVECTOR3(1, -1, 1);	//ライトが向いてる方向
	
	lightState.Diffuse.r = 1.0f;					//ライトの色
	lightState.Diffuse.g = 1.0f;
	lightState.Diffuse.b = 1.0f;

	pDevice->SetLight(0, &lightState);	//ライトを作成
	pDevice->LightEnable(0, TRUE);		//ライトをON

	//カメラ
	D3DXMATRIX view, proj;
	
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 7, 40), &D3DXVECTOR3(0, 0, 10), &D3DXVECTOR3(0, 1, 0));	//ビュー行列の作成

	pDevice->SetTransform(D3DTS_VIEW, &view);															//ビュー行列のセット
	
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 100000.0f);	//プロジェクション行列の作成
	pDevice->SetTransform(D3DTS_PROJECTION, &proj);																//プロジェクション行列のセット
	
	

}

void Direct3D::BeginDraw()
{
	//画面をクリア(毎フレーム)
	pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 255, 255), 1.0f, 0);

	//描画開始
	pDevice->BeginScene();
}

void Direct3D::EndDraw()
{
	//描画終了
	pDevice->EndScene();

	//スワップ
	pDevice->Present(NULL, NULL, NULL, NULL);
}

//開放処理
void Direct3D::Release()
{
	pDevice->Release();
	pD3d->Release();
}
