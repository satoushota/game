#pragma once
#include<d3dx9.h>
#include<list>
#include<string>
#include "Direct3D.h"
#include "Global.h"

class Collider;

//ゲームオブジェクトクラス
class IGameObject
{
protected:	

	//親の情報
	IGameObject* pParent_;
	
	//コライダー
	Collider* pCollider_;

	//衝突判定リスト
	std::list<Collider*> colliderList_;	

	//子供の情報
	std::list<IGameObject*> childList_;
	
	//オブジェクトの名前
	std::string name_;

	//位置、角度、大きさ
	D3DXVECTOR3 position_;
	D3DXVECTOR3 rotate_;
	D3DXVECTOR3 scale_;

	//自分の行列用
	D3DXMATRIX localMatrix_;

	//今いるシーンの行列
	D3DXMATRIX worldMatrix_;

	//時間
	double time_;

	//削除（）のフラグ
	bool dead_;

	//変形
	void Transform();

public:
	
	//コンストラクタ
	IGameObject();
	
	//引数:親のポインタ
	IGameObject(IGameObject* parent);

	//引数 : 親のポインタ、自分の名前
	IGameObject(IGameObject* parent, const std::string& name);
	
	//デストラクタ
	virtual ~IGameObject();

	//クラスを作る際のテンプレ
	template <class T>
	T* CreateGameObject(IGameObject* parent)
	{
		//親の情報を取得
		T* p = new T(parent);
		//親の子供情報にこれをいれる
		parent->PushBackChild(p);
		p->Initialize();
		return p;
	}

	//初期化
	virtual void Initialize() = 0;

	//更新、子クラスの更新用
	virtual void Update() = 0;
	void UpdateSub();

	//描画、子クラスの描画用
	virtual void Draw() = 0;
	void DrawSub();

	//開放、子クラスの解放用
	virtual void Release() = 0;
	void ReleaseSub();
	
	//当たり判定
	void Collision(IGameObject* targetOblect);
	
	//衝突処理
	//引数: 当たった相手,自分の衝突検知したコライダー,相手の衝突検知したコライダー
	virtual void OnCollision(IGameObject* pTarget, Collider* pMyCollider, Collider* pCollider) {};

	//テスト用の衝突判定枠を表示
	void CollisionDraw();

	//削除のフラグを立てる
	void KillMe();

	//子供の捜索
	//引数　: オブジェクトの名前
	//戻り値 : 指定したオブジェクトのアドレス
	IGameObject* FindChildObject(const std::string& name);

	void PushBackChild(IGameObject* obj);
		
	//各種ゲッター
	D3DXVECTOR3 GetPosition();

	D3DXVECTOR3 GetRotate();

	D3DXVECTOR3 GetScale();

	IGameObject* GetParent();

	Collider* GetCollider();

	std::list<IGameObject*>* GetChildList();

	const std::string& GetObjectName(void) const;

	//各種セッター
	void SetPosition(D3DXVECTOR3 position);

	void SetRotate(D3DXVECTOR3 rotate);

	void SetScale(D3DXVECTOR3 scale);

	void SetCollider(Collider* collider);

};

