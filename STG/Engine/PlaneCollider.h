#pragma once
#include "Collider.h"


class PlaneCollider : public Collider
{
	friend class Collider;

	//平面情報（a,b,c,d）
	D3DXVECTOR4 plane_;

	//法線どっちの面が正面か
	D3DXVECTOR3	normal_;

public:
	//引数: owner 持ち主 pos 平面が通る点　normal 法線（どの方向が正面か）
	PlaneCollider(IGameObject* owner, D3DXVECTOR3 pos,D3DXVECTOR3 normal);
	~PlaneCollider();

	bool IsHit(Collider* pTarget) override;

	D3DXVECTOR3 GetNormal();
};

