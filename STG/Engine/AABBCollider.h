#pragma once
#include "Collider.h"

class AABBCollider : public Collider
{
	friend class Collider;
	
	//AABBの最小値
	D3DXVECTOR3 boxMin_;

	//AABBの最大値
	D3DXVECTOR3 boxMax_;
	
public:
	//引数 : owner 持ち主,boxMax　AABBの大きさ（xyzの一辺の長さ）,center 箱の中心
	AABBCollider(IGameObject* owner,D3DXVECTOR3 boxMax,D3DXVECTOR3 center);
	~AABBCollider();

	bool IsHit(Collider* pTarget) override;
};

