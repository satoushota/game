#pragma once
#include "Global.h"
#include "IGameObject.h"

//形
enum ColliderShape
{
	SPHERE,
	PLANE,
	AABB,
	OBB,
};

class Collider
{
	friend	class PlaneCollider;
	friend  class SphereCollider;
	friend	class AABBCollider;
	friend  class OBBCollider;

protected:

	//誰のオブジェクトなのか
	IGameObject* owner_;

	//あたり判定の形
	ColliderShape shape_;

	//当たり判定の中心位置
	D3DXVECTOR3 center_;

	//テスト表示用の枠
	LPD3DXMESH	pMesh_;			

public:
	Collider();
	~Collider();
	
	//当たり判定
	//引数:相手のあたり判定
	//戻り値:当たったかどうか
	virtual bool IsHit(Collider* target) = 0;

	//球と球の当たり判定
	//引数:球判定と相手の球判定
	//戻り値:当たったたかどうか
	bool IsHitSphereAndSphere(SphereCollider* sphere1, SphereCollider* sphere2);


	//平面と球の当たり判定
	//引数: 平面判定と相手の球判定
	//戻り値:当たったかどうか
	bool IsHitPlaneAndSphere(PlaneCollider* plane, SphereCollider* sphere);

	//AABBと球の当たり判定
	//引数: AABBと相手の球の判定
	//戻り値:当たったかどうか
	bool IsHitAABBAndSphere(AABBCollider* box, SphereCollider* sphere);

	//OBBと球の当たり判定
	//引数　OBBと相手の球の判定
	//戻り値　当たったかどうか
	bool IsHitOBBAndSphere(OBBCollider* box, SphereCollider* sphere);

	//テスト表示用の枠を描画
	//引数：position	位置
	void Draw(D3DXVECTOR3 position);

	//他クラスが当たり判定の形を取得する際に使用する（ゲッター）
	ColliderShape GetShape();
};

