#include "IGameObject.h"
#include "SceneManager.h"
#include "Collider.h"

//コンストラクタ(親も名前もなし)
IGameObject::IGameObject() :
	IGameObject(nullptr, "")
{
}

//コンストラクタ（名前なし）
IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")
{
}

//コンストラクタ（標準）
IGameObject::IGameObject(IGameObject * parent, const std::string & name) :
	pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)),time_(0.0),
	pCollider_(nullptr),dead_(false)
{
	
}

//デストラクタ
IGameObject::~IGameObject()
{
	for (auto i = colliderList_.begin(); i != colliderList_.end(); i++)
	{
		SAFE_DELETE((*i));
	}

	SAFE_DELETE(pCollider_);
}

void IGameObject::Transform()
{
	//移動行列、回転行列(x,y,z)、拡大行列
	D3DXMATRIX matT, matRX, matRY, matRZ, matS;

	//移動行列の作成
	D3DXMatrixTranslation(&matT, position_.x, position_.y, position_.z);

	//回転行列の作成
	D3DXMatrixRotationX(&matRX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matRY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matRZ, D3DXToRadian(rotate_.z));

	//拡大行列の作成
	D3DXMatrixScaling(&matS, scale_.x, scale_.y, scale_.z);

	//各行列の合成
	localMatrix_ = matS * matRX * matRY *matRZ * matT;

	//
	if (pParent_ == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}
	else
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
	
}

//子の更新
void IGameObject::UpdateSub()
{
	Update();
	
	Transform();

	Collision(SceneManager::GetCurrentScene());

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{

		//削除フラグが真なら
		if ((*it)->dead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			it = childList_.erase(it);
		}
		else
		{
			it++;
		}
	}
}

//子の描画
void IGameObject::DrawSub()
{
	Draw();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
		(*it)->CollisionDraw();
	}
}

//子の解放
void IGameObject::ReleaseSub()
{
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}


void IGameObject::CollisionDraw()
{
	Direct3D::pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);	//ワイヤーフレーム
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);				//ライティングOFF
	Direct3D::pDevice->SetTexture(0, nullptr);								//テクスチャなし

	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		(*i)->Draw(position_);
	}

	Direct3D::pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING,true);
}

void IGameObject::KillMe()
{
	dead_ = true;
}

IGameObject * IGameObject::FindChildObject(const std::string & name)
{
	//子供がいないなら終わり
	if (childList_.empty())
	{
		return nullptr;
	}
		
	//イテレータ
	auto it = childList_.begin();	//先頭
	auto end = childList_.end();	//末尾

	//子オブジェクトから探す
	while (it != end)
	{
		//同じ名前のオブジェクトを見つけたらそれを返す
		if ((*it)->GetObjectName() == name)
		{
			return *it;
		}

		//その子供（孫）以降にいないか探す
		IGameObject* obj = (*it)->FindChildObject(name);
		if (obj != nullptr)
		{
			return obj;
		}

		//次の子へ
		it++;
	}

	//見つからなかった
	return nullptr;
}

void IGameObject::PushBackChild(IGameObject* pObj)
{
	childList_.push_back(pObj);
}

void IGameObject::Collision(IGameObject* targetObject)
{


	//当たったのは自分だったら処理はしない
	if (this == targetObject)
	{
		return;
	}
	
	//自分とtargetObjectのコリジョン情報を使って当たり判定
	//1つのオブジェクトが複数のコリジョン情報を持ってる場合もあるので二重ループ
	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		for (auto j = targetObject->colliderList_.begin(); j != targetObject->colliderList_.end(); j++)
		{
			if ((*i)->IsHit(*j))
			{
				
				//当たった
				OnCollision(targetObject,(*i),(*j));
			}
		}
	}

	//子供がいないなら終わり
	if (targetObject->childList_.empty())
		return;

	//子供も当たり判定
	for (auto i = targetObject->childList_.begin(); i != targetObject->childList_.end(); i++)
	{
		Collision(*i);
	}

}

const std::string & IGameObject::GetObjectName(void) const
{
	return name_;
}

IGameObject * IGameObject::GetParent()
{
	return pParent_;
}

Collider * IGameObject::GetCollider()
{
	return pCollider_;
}

std::list<IGameObject*>* IGameObject::GetChildList()
{
	return &childList_;
}

D3DXVECTOR3 IGameObject::GetRotate()
{
	return rotate_;
}

D3DXVECTOR3 IGameObject::GetScale()
{
	return scale_;
}

D3DXVECTOR3 IGameObject::GetPosition()
{
	return position_;
}

void IGameObject::SetPosition(D3DXVECTOR3 position)
{
	position_ = position;
}

void IGameObject::SetRotate(D3DXVECTOR3 rotate)
{
	rotate_ = rotate;
}

void IGameObject::SetScale(D3DXVECTOR3 scale)
{
	scale_ = scale;
}

void IGameObject::SetCollider(Collider* collider)
{
	colliderList_.push_back(collider);
}