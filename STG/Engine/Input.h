#pragma once

#include <d3dx9.h>
#include <dInput.h>


#pragma comment(lib, "dxguid.lib")
#pragma comment(lib, "dInput8.lib")

#define SAFE_RELEASE(p) if(p != nullptr){ p->Release(); p = nullptr;} 

namespace Input
{
	void Initialize(HWND hWnd);
	
	void Update();
	
	bool IsKey(int keyCode); 
	
	bool IsKeyDown(int keyCode);
	
	bool IsKeyUp(int keyCode);	
	
	bool IsMouseButton(int buttonCode);

	bool IsMouseButtonDown(int buttonCode);

	bool IsMouseButtonUp(int buttonCode);

	//マウスカーソルの位置を取得
	//戻値：マウスカーソルの位置
	D3DXVECTOR3 GetMousePosition();

	//そのフレームでのマウスの移動量を取得
	//戻値：X,Y マウスの移動量 ／ Z,ホイールの回転量
	D3DXVECTOR3 GetMouseMove();

	void Release();


};