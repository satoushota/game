#pragma once
#include "Collider.h"

class SphereCollider :public Collider
{
	friend class Collider;

	//球の中心
	D3DXVECTOR3 center_;

	//球の過去の位置（当たり判定のすりぬけ防止用）
	D3DXVECTOR3 prePos_;

	//半径
	float radius_;

public:
	SphereCollider(IGameObject* owner, D3DXVECTOR3 center, float radius);
	~SphereCollider();

	bool IsHit(Collider* pTarget) override;

	D3DXVECTOR3 GetCollisionPos();

	void SetPrePos(D3DXVECTOR3 prepos);


};
