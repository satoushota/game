#pragma once
#include <string>
#include "Fbx.h"

namespace Model
{
	struct ModelData
	{
		std::string fileName;
		Fbx* pFbx;
		D3DXMATRIX matrix;

		//コンストラクタ
		ModelData() : fileName(""), pFbx(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	int Load(std::string filename);
	void Draw(int handle);

	void SetMatrix(int handle, D3DXMATRIX& matrix);
	void Release(int handle);
	void AllRelase();

}

