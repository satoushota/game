#include "Camera.h"
#include"Direct3D.h"

//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"), target_(D3DXVECTOR3(0,0,0))
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	D3DXMATRIX proj;
	//プロジェクション行列をつくるための関数（１、入れる箱　２　視野角(ズーム)　３、アスペクト比 4,どこから先を表示するか(ニアクリッピング面),５何メートル先まで移すか（ファークリッピング面）
	//４、５の差はなるべくないほうがいい
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(103), (float)g.screenWidth / g.screenHeight, 0.5f, 1000.0f);
	//
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void Camera::Update()
{
	Transform();


	D3DXVECTOR3 worldPosition, worldTarget;
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

	D3DXMATRIX view;
	//ビュー行列を作るための関数(１、入れる箱　２、カメラの位置（視点）３、どこをみるか（焦点） 4上方向ベクトル（上を示す）)
	D3DXMatrixLookAtLH(&view, &worldPosition,&worldTarget, &D3DXVECTOR3(0, 1, 0));
	//(1,ビュー行列として使ってほしい情報)
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);

}

//描画
void Camera::Draw()
{
}

//開放
void Camera::Release()
{
}