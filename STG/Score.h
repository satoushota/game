#pragma once
#include "Engine/Global.h"
#include <string>

//スコアを管理するクラス
enum 
{
	RESULT_SCOREBOARD,
	RESULT_HIGHSCOREBOARD,
	RESULT＿KEYGUIDE,
	BACK_GROUND,
	SCOREUPADATE,
	SCORE_IMAGE_MAX
};

enum SCORE_NUMBER
{
	SCORE_0,
	SCORE_1,
	SCORE_2,
	SCORE_3,
	SCORE_4,
	SCORE_5,
	SCORE_6,
	SCORE_7,
	SCORE_8,
	SCORE_9,
	SCORE_NUMBER_MAX
};

class Score : public IGameObject
{
	//画像の管理番号
	int hImage_[SCORE_IMAGE_MAX];

	//スコアナンバー表示用の画像配列
	int hNumImage_[SCORE_NUMBER_MAX];

	//スコア
	unsigned int score_;

	//ハイスコア
	unsigned int highScore_;

	//ハイスコア更新をしたかどうか
	bool isHighScoreUpdate_;

	//スコアを表示するかどうか
	bool scoreShowFlag_;

	//ハイスコアを表示するかどうか
	bool highScoreShowFlag_;

	//ハイスコア更新を表示するかどうか
	bool highScoreUpdateShowFlag_;

public:
	//コンストラクタ
	Score(IGameObject* parent);

	//デストラクタ
	~Score();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//スコアを加算
	//引数 score 加算するスコアポイント
	void AddScore(unsigned int score);

	//スコアを表示
	void ShowScore();

	//スコアの読み取り
	//引数 :スコアファイルの名前
	void ReadScoreFile(std::string fileName);

	//スコアを記録
	//引数 :スコアファイルの名前
	void SaveScoreFile(std::string fileName);

	//ハイスコアを読み取り
	//引数 :スコアファイルの名前
	void ReadHighScoreFile(std::string fileName);

	//ハイスコアを記録
	//引数 :スコアファイルの名前
	void SaveHighScoreFile(std::string fileName);

	//ハイスコアを表示
	//引数 :スコアファイルの名前
	void ShowHighScore();

	//セッター	
	//スコアを表示するかしないかのフラグを決めるために使用する
	void SetScoreShowFlag(bool flag);

	//ハイスコアを表示するかしないかのフラグを決めるために使用する
	void SetHighScoreShowFlag(bool flag);

	//ハイスコア更新表示するかしないかのフラグを決めるために使用する
	void SetHighScoreUpdateShowFlag(bool flag);

	//ゲッター
	
	//現在のスコア表示するかしないかのフラグを確認するために使用
	bool GetScoreShowFlag();

	//現在のハイスコア表示するかしないかのフラグを確認するために使用
	bool GetHighScoreShowFlag();

	//現在のハイスコア更新表示するかしないかのフラグを確認するために使用
	bool GetHighScoreUpdateFlag();
};