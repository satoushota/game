#pragma once
#include "Engine/Global.h"

const int TOY_COUNT_MAX = 8;

//景品（おもちゃ）を管理するクラス
class Toy : public IGameObject
{
	int hModel_;
public:
	//コンストラクタ
	Toy(IGameObject* parent);

	//デストラクタ
	~Toy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//衝突処理
	void OnCollision(IGameObject* pTarget, Collider* pMyCollider, Collider* pCollider) override;
};

