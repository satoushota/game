#pragma once
#include "Engine/Global.h"
#include "Bullet.h"

class Bullet;

enum
{

};

//銃を管理するクラス
class Rifle : public IGameObject
{
	//画像の識別番号
	int hImage_;

	//残段数
	unsigned int bulletCount_;

	//弾を管理するためのポインタ
	Bullet* pBullet_;

	//銃口の向き
	D3DXVECTOR3 muzzleDir_;

	//弾が飛ぶ方向
	D3DXVECTOR3 bulletMove_;

public:
	//コンストラクタ
	Rifle(IGameObject* parent);

	//デストラクタ
	~Rifle();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void CreateBulletMove();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ゲッター
	D3DXVECTOR3 GetBulletMove_();
};