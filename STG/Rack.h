#pragma once
#include "Engine/Global.h"



//棚を管理するクラス
class Rack : public IGameObject
{
	int hModel_;
public:
	//コンストラクタ
	Rack(IGameObject* parent);

	//デストラクタ
	~Rack();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};