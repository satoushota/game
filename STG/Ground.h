#pragma once
#include "Engine/Global.h"


//地面を管理するクラス
class Ground : public IGameObject
{
	//モデルの識別番号
	int hModel_;

public:
	//コンストラクタ
	Ground(IGameObject* parent);

	//デストラクタ.00
	~Ground();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

};


